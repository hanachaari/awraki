import 'dart:typed_data';
import 'package:communication/signature_service.dart';
import 'package:convert/convert.dart';
import 'package:pointycastle/digests/blake2b.dart';
import 'package:tezart/tezart.dart';
import '../../shared.dart';
Future<void> reveal(Keystore keyStore) async {
  final client = TezartClient(Shared.rpcNode);
  final operation = client.revealKeyOperation(keyStore);
  try {
    await operation.executeAndMonitor();
  } catch (e) {
    rethrow;
  }
}

Future<bool> checkReveal(Keystore keyStore) async {
  final client = TezartClient(Shared.rpcNode);
  return await client.isKeyRevealed(keyStore.address);
}



