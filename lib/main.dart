import 'package:tezart/tezart.dart';
import 'package:communication/tezos_service.dart';
import '../../shared.dart';

Future<void> main() async {

  /// Generate keystore from mnemonic
  var keystore = Keystore.fromMnemonic(Shared.mnemonic,email: Shared.email,password: Shared.password);
  // Sample output of keystore created from mnemonic
  print(keystore);
  ///reveal(keystore);

  print("secret --- "+ keystore.secretKey);

  print("public ---- "+keystore.publicKey);

  print("address --- "+keystore.address);

  final isReveal=await checkReveal(keystore);
  if (!isReveal) throw Exception("account don't exist on blockchain");
  final client = TezartClient(Shared.rpcNode);
  final rpcInterface = client.rpcInterface;
  final contract = Contract(contractAddress:Shared.contractAddress, rpcInterface: rpcInterface);
  final callContractOperationsList = await contract.callOperation(
    entrypoint: 'add_user',
    source:  keystore,
  );
  await callContractOperationsList.executeAndMonitor();

}